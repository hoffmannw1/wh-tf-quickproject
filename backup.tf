# backup.txt
### A project as DR projt
## TODO :
## - Create a DR-VPC
## - Create a GCS bucket to store backup

resource "google_project" "project-dr" {
  name                = "${local.project_id}-dr"
  project_id          = "${local.project_id}-dr"
  billing_account     = var.billing_account
  auto_create_network = false
}

# Activation metadata :
resource "google_compute_project_metadata" "enableosconfig-dr" {
  metadata = {
    enable-osconfig         = "true"
    enable-oslogin          = "true"
    enable-os-inventory     = "true"
    enable-guest-attributes = "true"
  }
  project = google_project.project-dr.project_id
}

## See : https://cloud.google.com/backup-disaster-recovery/docs/create-plan/dynamic-protection#terraform

resource "google_compute_network" "vpc_network_backup" {
  name                    = "${local.project_id}-vpcbackup"
  project                 = google_project.project.project_id
  auto_create_subnetworks = false
}

# Création des subnets
resource "google_compute_subnetwork" "backup_subnet1" {
  name          = "${local.project_id}-backupsubnet1"
  ip_cidr_range = "10.128.0.0/20"
  network       = google_compute_network.vpc_network_backup.id
  region        = var.region1
  project       = google_project.project.project_id
  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "backup_subnet2" {
  name          = "${local.project_id}-backupsubnet2"
  ip_cidr_range = "10.132.0.0/20"
  network       = google_compute_network.vpc_network_backup.id
  region        = var.region2
  project       = google_project.project.project_id
  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_global_address" "private_ip_address" {
  provider      = google-beta # Keep it as beta if it's not in the stable provider yet
  project       = google_project.project.project_id
  name          = "vpc-network-backup"
  address_type  = "INTERNAL"
  purpose       = "VPC_PEERING"
  prefix_length = 20
  network       = google_compute_network.vpc_network_backup.id
}

resource "google_service_networking_connection" "default" {
  provider                = google-beta # Keep it as beta if it's not in the stable provider yet
  network                 = google_compute_network.vpc_network_backup.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_backup_dr_management_server" "ms_console" {
  project  = google_project.project.project_id
  provider = google-beta
  location = var.region4 # Consider adding a variable for the backup region
  name     = "ms-console"
  type     = "BACKUP_RESTORE"
  networks {
    network      = google_compute_network.vpc_network_backup.id
    peering_mode = "PRIVATE_SERVICE_ACCESS"
  }
  depends_on = [google_service_networking_connection.default]
}

## Tech preview / Do not use for the moment.
resource "google_backup_dr_backup_vault" "backup_vault_west1" {
  provider                                   = google-beta
  location                                   = "europe-west1"
  backup_vault_id                            = "backup-vault-west1"
  description                                = "This is a backup vault built by Terraform."
  backup_minimum_enforced_retention_duration = "100000s"
  force_update                               = true
  allow_missing                              = true
  project                                    = google_project.project.project_id
  depends_on                                 = [google_project_service.api]
}

resource "google_backup_dr_backup_vault" "backup_vault_west4" {
  provider                                   = google-beta
  location                                   = "europe-west4"
  backup_vault_id                            = "backup-vault-west4"
  description                                = "This is a  backup vault built by Terraform."
  backup_minimum_enforced_retention_duration = "100000s"
  force_update                               = true
  allow_missing                              = true
  project                                    = google_project.project.project_id
  depends_on                                 = [google_project_service.api]
}

resource "google_backup_dr_backup_plan" "backup_plan1" {
  provider       = google-beta
  project        = google_project.project.project_id
  location       = "europe-west1"
  resource_type  = "compute.googleapis.com/Instance"
  backup_plan_id = "${local.project_id}-backup-plan-west1"
  backup_vault   = "projects/${google_project.project.project_id}/locations/europe-west1/backupVaults/${google_backup_dr_backup_vault.backup_vault_west1.name}"

  backup_rules {
    rule_id               = "backup-rule1-west1"
    backup_retention_days = 5

    standard_schedule {
      recurrence_type  = "HOURLY"
      hourly_frequency = 6
      time_zone        = "UTC" # Consider using a variable for timezone
      backup_window {
        start_hour_of_day = 0
        end_hour_of_day   = 24
      }
    }
  }
}

resource "google_backup_dr_backup_plan" "backup_plan2" {
  provider       = google-beta
  project        = google_project.project.project_id
  location       = "europe-west4"
  resource_type  = "compute.googleapis.com/Instance"
  backup_plan_id = "${local.project_id}-backup-plan-west4"
  backup_vault   = "projects/${google_project.project.project_id}/locations/europe-west4/backupVaults/${google_backup_dr_backup_vault.backup_vault_west4.name}"

  backup_rules {
    rule_id               = "backup-rule1-west4"
    backup_retention_days = 5

    standard_schedule {
      recurrence_type  = "HOURLY"
      hourly_frequency = 6
      time_zone        = "UTC" # Consider using a variable for timezone
      backup_window {
        start_hour_of_day = 0
        end_hour_of_day   = 24
      }
    }
  }
}
