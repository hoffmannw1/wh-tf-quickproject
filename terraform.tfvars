# terraform.tfvars.txt
region1           = "europe-west1"
region2           = "europe-west4"
zone1             = "europe-west1-b"
zone2             = "europe-west4-b"
nb_vm-linux_zone1 = 3
nb_vm-linux_zone2 = 4
nb_vm-win_zone1   = 1
nb_vm-win_zone2   = 1
nb_vm-pgsql_zone1 = 1
region4           = "europe-west4" # Example for backup region
# billing_account = "VOTRE_ID_COMPTE_FACTURATION"  # Définir la variable d'environnement à la place
# export TF_VAR_billing_account=XXXX-XXXX-XXXX-XXXX # Remplacez par votre ID

