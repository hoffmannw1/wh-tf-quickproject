# main.txt
# quick and dirty to deploy some workloads

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.50.0"
    }
    #    google-beta = {
    #      source  = "hashicorp/google-beta"
    #      version = ">= 4.0"
    #    }
  }
}

provider "google" {
  region = var.region1
  zone   = var.zone1
}

provider "google-beta" {
  region = var.region1 # Ou la région appropriée pour les ressources beta
}

resource "random_string" "prefix" {
  length  = 6
  special = false
  upper   = false
  numeric = false
}

locals {
  project_id = "wh-${random_string.prefix.result}"
  common_tags = {
    environment = "prod" # Example tag, adjust as needed
  }
  // }
  log_bucket_name = "${local.project_id}-logs"
}

# Création du projet GCP
resource "google_project" "project" {
  name                = local.project_id
  project_id          = local.project_id
  billing_account     = var.billing_account
  auto_create_network = false
}

# Activation metadata :
resource "google_compute_project_metadata" "enableosconfig" {
  metadata = {
    enable-osconfig         = "true"
    enable-oslogin          = "true"
    enable-os-inventory     = "true"
    enable-guest-attributes = "true"
  }
  project = google_project.project.project_id
}

# Enable required APIs
resource "google_project_service" "api" {
  for_each                   = toset(concat(var.apis_to_enable, var.apisbeta_to_enable))
  service                    = each.key
  project                    = google_project.project.project_id
  disable_dependent_services = true
  disable_on_destroy         = false
}

# Création SA pour les VMs
resource "google_service_account" "vms_sa" {
  account_id   = "${local.project_id}-vms-sa"
  display_name = "Service Account for VM"
  project      = google_project.project.project_id
}

resource "google_project_iam_member" "monitoring_metric_writer" {
  project = google_project.project.project_id
  role    = "roles/monitoring.metricWriter"
  member  = "serviceAccount:${google_service_account.vms_sa.email}"
}

resource "google_project_iam_member" "logging_log_writer" {
  project = google_project.project.project_id
  role    = "roles/logging.logWriter"
  member  = "serviceAccount:${google_service_account.vms_sa.email}"
}

# Création du bucket de logs
#resource "google_storage_bucket" "log_bucket" {
#  project       = google_project.project.project_id
#  name          = local.log_bucket_name
#  location      = var.region1
#  force_destroy = false # Changer à true avec prudence, permet de supprimer un bucket non vide
#
#  # Appliquer l'accès uniforme au niveau du bucket pour simplifier la gestion IAM
#  uniform_bucket_level_access = true
#
#  # Empêcher la suppression accidentelle
#  # Pour activer cette protection, vous devrez d'abord supprimer cette section,
#  # appliquer la configuration, puis la réajouter et réappliquer.
#  # deletion_protection = true
#}
resource "google_logging_project_bucket_config" "analytics-enabled-bucket" {
  project          = google_project.project.project_id
  location      = var.region1
  # location         = "global"
  retention_days   = 30
  enable_analytics = true
  bucket_id        = "custom-bucket"
  index_configs {
    field_path = "jsonPayload.request.status"
    type       = "INDEX_TYPE_STRING"
  }
}
