# network.txt
### NETWORK ###
#
# Création du VPC principal
resource "google_compute_network" "vpc" {
  name                    = "${local.project_id}-vpc"
  project                 = google_project.project.project_id
  auto_create_subnetworks = false
}

# Création des subnets
resource "google_compute_subnetwork" "subnet1" {
  name          = "${local.project_id}-subnet1"
  ip_cidr_range = "172.16.0.0/25"
  network       = google_compute_network.vpc.id
  region        = var.region1
  project       = google_project.project.project_id
  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "subnet2" {
  name          = "${local.project_id}-subnet2"
  ip_cidr_range = "172.16.0.128/25"
  network       = google_compute_network.vpc.id
  region        = var.region2
  project       = google_project.project.project_id
  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
  depends_on = [google_compute_network.vpc] # Explicit dependency
}

# Enable Flow Logs on Subnets (Recommended way)
resource "google_compute_subnetwork" "subnet1_with_flows" {
  name          = google_compute_subnetwork.subnet1.name
  region        = google_compute_subnetwork.subnet1.region
  network       = google_compute_network.vpc.id
  ip_cidr_range = google_compute_subnetwork.subnet1.ip_cidr_range
  project       = google_project.project.project_id
  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "subnet2_with_flows" {
  name          = google_compute_subnetwork.subnet2.name
  region        = google_compute_subnetwork.subnet2.region
  network       = google_compute_network.vpc.id
  ip_cidr_range = google_compute_subnetwork.subnet2.ip_cidr_range
  project       = google_project.project.project_id
  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
  depends_on = [google_compute_network.vpc] # Explicit dependency
}

# Créer un Cloud Router region1
resource "google_compute_router" "cloudrouter1" {
  name       = "${local.project_id}-router1"
  region     = var.region1
  network    = google_compute_network.vpc.name
  project    = google_project.project.project_id
  depends_on = [google_compute_network.vpc] # Explicit dependency
}

# Créer un Cloud Router region2
resource "google_compute_router" "cloudrouter2" {
  name       = "${local.project_id}-router2"
  region     = var.region2
  network    = google_compute_network.vpc.name
  project    = google_project.project.project_id
  depends_on = [google_compute_network.vpc] # Explicit dependency
}

# Créer un Cloud NAT region1
resource "google_compute_router_nat" "cloudnat1" {
  name                               = "${local.project_id}-cloudnat1"
  router                             = google_compute_router.cloudrouter1.name
  region                             = google_compute_router.cloudrouter1.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  project                            = google_project.project.project_id
  depends_on                         = [google_compute_router.cloudrouter1] # Explicit dependency
}

# Créer un Cloud NAT region2
resource "google_compute_router_nat" "cloudnat2" {
  name                               = "${local.project_id}-cloudnat2"
  router                             = google_compute_router.cloudrouter2.name
  region                             = google_compute_router.cloudrouter2.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  project                            = google_project.project.project_id
  depends_on                         = [google_compute_router.cloudrouter2] # Explicit dependency
}

### FIREWALL
resource "google_compute_firewall" "allow_essential" {
  name    = "allow-essential-${local.project_id}"
  network = google_compute_network.vpc.id
  project = google_project.project.project_id

  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443"] # Example: Allow SSH, HTTP, HTTPS
  }

  source_ranges = ["0.0.0.0/0"] # Consider more restrictive source ranges
}
### / FIREWALL
### / NETWORK ###
