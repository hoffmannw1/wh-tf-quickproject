# variables.txt
variable "billing_account" {
  type        = string
  description = "ID du compte de facturation GCP"
}

variable "region1" {
  type        = string
  description = "Région GCP (ex: europe-west9)"
}

variable "region2" {
  type        = string
  description = "Région GCP (ex: europe-west9)"
}
variable "region4" {
  type        = string
  description = "Région GCP (ex: europe-west9)"
}
variable "zone1" {
  type        = string
  description = "Zone GCP (ex: europe-west9-b)"
}

variable "zone2" {
  type        = string
  description = "Zone GCP (ex: europe-west6-b)"
}
variable "nb_vm-linux_zone1" {
  type        = number
  description = "Nombre de VMs à créer dans la zone 1"
  default     = 1
}
variable "nb_vm-pgsql_zone1" {
  type        = number
  description = "Nombre de VMs PGSQL à créer dans la zone 1"
  default     = 1
}

variable "nb_vm-linux_zone2" {
  type        = number
  description = "Nombre de VMs à créer dans la zone 2"
  default     = 1
}

variable "nb_vm-win_zone1" {
  type        = number
  description = "Nombre de VMs à créer dans la zone 1"
  default     = 1
}

variable "nb_vm-win_zone2" {
  type        = number
  description = "Nombre de VMs à créer dans la zone 2"
  default     = 1
}
variable "apis_to_enable" {
  type = list(string)
  default = [
    "monitoring.googleapis.com",
    "logging.googleapis.com",
    "osconfig.googleapis.com",
    "networkmanagement.googleapis.com",
    "servicenetworking.googleapis.com",
    "backupdr.googleapis.com",
    "container.googleapis.com",
    "cloudkms.googleapis.com",
    "workflows.googleapis.com"
  ]
}
variable "apisbeta_to_enable" {
  type = list(string)
  default = [
    # No need to duplicate osconfig if it's in apis_to_enable
  ]
}
variable "timezone" {
  type        = string
  description = "Timezone for scheduled tasks"
  default     = "Europe/Paris"
}
