# vmwin.txt
### Création des VMs
## Zone 1
resource "google_compute_instance" "vm_win_zone1" {
  count        = var.nb_vm-win_zone1
  name         = "${local.project_id}-vmwin-zone1-${count.index + 1}"
  machine_type = "e2-micro"
  zone         = var.zone1
  tags         = ["wh-allow-all"]
  labels = merge(local.common_tags, {
    ostype = "windows"
    backup = "true"
  })
  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }
  project    = google_project.project.project_id
  depends_on = [google_project.project]
  boot_disk {
    initialize_params {
      image = "windows-cloud/windows-2019-core"
    }
  }
  network_interface {
    subnetwork = google_compute_subnetwork.subnet1.id
  }
  service_account {
    email  = google_service_account.vms_sa.email
    scopes = ["monitoring", "logging-write"]
  }
}

## Zone 2
resource "google_compute_instance" "vm_win_zone2" {
  count        = var.nb_vm-win_zone2
  name         = "${local.project_id}-vmwin-zone2-${count.index + 1}"
  machine_type = "e2-micro"
  zone         = var.zone2
  tags         = ["wh-allow-all"]
  labels = merge(local.common_tags, {
    ostype = "windows"
    backup = "true"
  })
  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }
  project    = google_project.project.project_id
  depends_on = [google_project.project]
  boot_disk {
    initialize_params {
      image = "windows-cloud/windows-2019-core"
    }
  }
  network_interface {
    subnetwork = google_compute_subnetwork.subnet2.id
  }
  service_account {
    email  = google_service_account.vms_sa.email
    scopes = ["monitoring", "logging-write"]
  }
}

resource "google_os_config_patch_deployment" "patch_deployment_windows" {
  patch_deployment_id = "${local.project_id}-patch-deploymentwindows"
  project             = google_project.project.project_id
  instance_filter {
    group_labels {
      labels = {
        ostype = "windows"
        backup = "true"
      }
    }
  }
  recurring_schedule {
    time_zone {
      id = var.timezone
    }
    time_of_day {
      hours   = 0
      minutes = 30
      seconds = 30
      nanos   = 20
    }
  }
  patch_config {
    reboot_config = "NEVER"
    windows_update {
      classifications = ["CRITICAL", "SECURITY", "UPDATE"]
    }
  }
  rollout {
    mode = "ZONE_BY_ZONE"
    disruption_budget {
      percentage = 1
    }
  }
}

