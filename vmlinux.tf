# vmlinux.txt
### Création des VMs
## Zone 1
resource "google_compute_instance" "vm_linux_zone1" {
  count        = var.nb_vm-linux_zone1
  name         = "${local.project_id}-vm-zone1-${count.index + 1}"
  machine_type = "e2-micro"
  zone         = var.zone1
  tags         = ["wh-allow-all"]
  labels = merge(local.common_tags, {
    ostype = "linux"
    backup = "true"
  })
  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }
  project    = google_project.project.project_id
  depends_on = [google_project.project]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet1.id
  }

  service_account {
    email  = google_service_account.vms_sa.email
    scopes = ["monitoring", "logging-write"]
  }
}

## Zone 2
resource "google_compute_instance" "vm_linux_zone2" {
  count        = var.nb_vm-linux_zone2
  name         = "${local.project_id}-vm-zone2-${count.index + 1}"
  machine_type = "e2-micro"
  zone         = var.zone2
  tags         = ["wh-allow-all"]
  labels = merge(local.common_tags, {
    ostype = "linux"
    backup = "true"
  })
  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }

  project    = google_project.project.project_id
  depends_on = [google_project.project]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet2.id
  }

  service_account {
    email  = google_service_account.vms_sa.email
    scopes = ["monitoring", "logging-write"]
  }
}

### OS_CONFIG ###
resource "google_os_config_guest_policies" "debian_policy" {
  provider        = google-beta
  project         = google_project.project.project_id
  guest_policy_id = "debian-cloud-policy"
  assignment {
    os_types {
      os_short_name = "debian"
      os_version    = "11"
    }
    #  zones = ["europe-west6-b", "europe-west9-b"]
  }

  package_repositories {
    apt {
      uri          = "https://packages.cloud.google.com/apt"
      archive_type = "DEB"
      distribution = "google-compute-engine-bullseye"
      components   = ["main"]
    }
  }
  package_repositories {
    apt {
      uri          = "https://packages.cloud.google.com/apt"
      archive_type = "DEB"
      distribution = "cloud-sdk-bullseye"
      components   = ["main"]
    }
  }
  package_repositories {
    apt {
      uri          = "https://packages.cloud.google.com/apt"
      archive_type = "DEB"
      distribution = "google-cloud-ops-agent-bullseye-all"
      components   = ["main"]
    }
  }
  packages {
    name          = "google-compute-engine"
    desired_state = "UPDATED"
  }

  packages {
    name          = "google-compute-engine-oslogin"
    desired_state = "UPDATED"
  }

  packages {
    name          = "google-guest-agent"
    desired_state = "UPDATED"
  }

  packages {
    name          = "google-osconfig-agent"
    desired_state = "UPDATED"
  }
  packages {
    name          = "google-cloud-ops-agent"
    desired_state = "UPDATED"
  }
}

resource "google_os_config_patch_deployment" "patch_deployment_linux" {
  patch_deployment_id = "${local.project_id}-patch-deploymentlinux"
  project             = google_project.project.project_id
  instance_filter {
    #    all = true
    group_labels {
      labels = {
        ostype = "linux"
      }
    }
  }

  recurring_schedule {
    time_zone {
      id = var.timezone
    }
    time_of_day {
      hours   = 0
      minutes = 30
      seconds = 30
      nanos   = 20
    }
  }

  patch_config {
    reboot_config = "NEVER"
    apt {
      type     = "UPGRADE"
      excludes = ["python"]
    }
  }
  rollout {
    mode = "ZONE_BY_ZONE"
    disruption_budget {
      percentage = 1
    }
  }
}

### / OS_CONFIG ###

### OS_POLICY_ASSIGNEMENT ###

resource "google_os_config_os_policy_assignment" "debian_11_policy9" {
  location           = var.zone1
  project            = google_project.project.project_id
  name               = "debian-11-policy9"
  skip_await_rollout = "true"

  instance_filter {
    all = false
    inventories {
      os_short_name = "debian"
      os_version    = "11"
    }
  }
  rollout {
    disruption_budget {
      fixed = 1
    }
    min_wait_duration = "30s"
  }

  os_policies {
    id   = "debian11-policy"
    mode = "ENFORCEMENT"
    resource_groups {
      resources {
        id = "policy-nginx"
        pkg {
          desired_state = "INSTALLED"
          apt {
            name = "nginx"
          }
        }
      }
    }
  }
}

resource "google_os_config_os_policy_assignment" "debian_11_policy6" {
  location           = var.zone2
  project            = google_project.project.project_id
  name               = "debian-11-policy6"
  skip_await_rollout = "true"

  instance_filter {
    all = false
    inventories {
      os_short_name = "debian"
      os_version    = "11"
    }
  }
  rollout {
    disruption_budget {
      fixed = 1
    }
    min_wait_duration = "30s"
  }

  os_policies {
    id   = "debian11-policy"
    mode = "ENFORCEMENT"
    resource_groups {
      resources {
        id = "policy-nginx"
        pkg {
          desired_state = "INSTALLED"
          apt {
            name = "nginx"
          }
        }
      }
    }
  }
}
