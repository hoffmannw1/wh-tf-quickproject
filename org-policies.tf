# org-policies.txt
#
# Overwrite org policies in some secured environments.
#
resource "google_project_service" "org_services" {
  project = google_project.project.project_id
  for_each = toset([
    "compute.googleapis.com",
    "cloudbilling.googleapis.com",
    "cloudbuild.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
    "serviceusage.googleapis.com"
  ])
  service                    = each.key
  disable_dependent_services = true
}

resource "google_project_organization_policy" "disable_without_constraints" {
  project = google_project.project.project_id
  for_each = toset([
    "compute.requireShieldedVm",
    "compute.requireOsLogin",
    "iam.disableServiceAccountKeyCreation",
    "iam.disableServiceAccountCreation"
  ])
  constraint = "constraints/${each.key}"
  boolean_policy {
    enforced = false
  }
}

resource "google_project_organization_policy" "disable_with_constraints" {
  project = google_project.project.project_id
  for_each = toset([
    "compute.vmExternalIpAccess",
    "compute.restrictSharedVpcSubnetworks",
    "compute.restrictSharedVpcHostProjects",
    "compute.restrictVpcPeering",
    "compute.vmCanIpForward"
  ])
  constraint = "constraints/${each.key}"
  list_policy {
    allow {
      all = true
    }
  }
}
