# vm-sql.txt
## Zone 1 SQL
resource "google_compute_instance" "vm_pgsql" {
  count        = var.nb_vm-pgsql_zone1
  name         = "${local.project_id}-vm-pgsql-zone1-${count.index + 1}"
  machine_type = "e2-small"
  zone         = var.zone1
  tags         = ["wh-allow-all"]
  labels = merge(local.common_tags, {
    ostype = "linux"
    backup = "true"
  })

  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }

  project    = google_project.project.project_id
  depends_on = [google_project.project]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet1.id
  }

  service_account {
    email  = google_service_account.vms_sa.email
    scopes = ["monitoring", "logging-write"]
  }
  # --- Début de la configuration PostgreSQL ---

  metadata_startup_script = <<EOF
#!/bin/bash
sudo apt-get update
sudo apt-get install -y postgresql postgresql-contrib
sudo systemctl enable postgresql
sudo systemctl start postgresql

# Configuration de la sécurité (à adapter selon vos besoins)
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/14/main/postgresql.conf
sudo sed -i "s/#password_encryption = md5/password_encryption = scram-sha-256/g" /etc/postgresql/14/main/postgresql.conf

# Création d'un utilisateur et d'une base de données (à adapter)
sudo -u postgres psql -c "CREATE USER your_user WITH PASSWORD 'your_password';"
sudo -u postgres psql -c "CREATE DATABASE your_database OWNER your_user;"
EOF

  # --- Fin de la configuration PostgreSQL ---
}
